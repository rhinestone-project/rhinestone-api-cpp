/**
 * Copyright 2020 rhinestone-project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * */

//
// Created by egor9814 on 6/26/20, 20:12:27.
//


#ifndef __rhinestone_api_DBA0D4CF6E944D8C9C5C128163D467BB_hpp__
#define __rhinestone_api_DBA0D4CF6E944D8C9C5C128163D467BB_hpp__

#define RS_FAIL 0
#define RS_OK 1

#define RS_API
#define RS_API_LOAD_LIB int __LoadRSLibrary__(::rhinestone::api::rs_env_t env)
#define RS_API_UNLOAD_LIB int __UnloadRSLibrary__(::rhinestone::api::rs_env_t env)

#define RS_STRING(VALUE) u"" VALUE
#define RS_CHAR(VALUE) u##VALUE

//#define RS_ENABLE_RSPTR_FROM_THIS()\
//template <typename X> friend class ::rhinestone::api::rs_ptr;

#include <atomic>
#include <type_traits>
#include <cstddef>
#include <string>

namespace rhinestone::api {

    /// shared pointer implementation
    namespace __private__ {
        struct IRefCounter {
            using type = std::atomic_uint_fast64_t;
            using int_type = typename type::value_type;

            type count{0};

            IRefCounter() = default;

            IRefCounter(const IRefCounter &) = delete;
            IRefCounter(IRefCounter &&) noexcept = delete;
            IRefCounter &operator=(const IRefCounter &) = delete;
            IRefCounter &operator=(IRefCounter &&) noexcept = delete;

            virtual ~IRefCounter() = default;

            int_type add() {
                return count.fetch_add(1) + 1;
            }

            int_type sub() {
                auto c = count.fetch_sub(1) - 1;
                if (c == 0) {
                    onReleaseReference();
                    delete this;
                }
                return c;
            }

        private:
            virtual void onReleaseReference() = 0;
        };

        template <typename T>
        struct IMemoryStorage {
            using aligned_type = typename std::aligned_storage<sizeof(T), alignof(T)>::type;

            aligned_type storage;

            unsigned char &data{storage.__data[0]};

            T *get() const {
                return reinterpret_cast<T*>(&data);
            }

            template <typename ... Args>
            T *allocate(Args &&... args) {
                new (&data) T(std::forward<Args>(args)...);
                return get();
            }

            void destruct() {
                get()->~T();
                for (std::size_t i = 0; i < sizeof(storage.__data); i++) {
                    storage.__data[i] = 0;
                }
            }
        };

        template <typename T>
        struct TypedRefCounter : IMemoryStorage<T>, IRefCounter {
            using storage_type = IMemoryStorage<T>;

        private:
            void onReleaseReference() override {
                storage_type::destruct();
            }
        };

        template <typename T>
        struct BasePointer {
            T *_instance{nullptr};
            IRefCounter *_refCounter{nullptr};

            BasePointer() = default;

            BasePointer(T *instance, IRefCounter *refCounter) : _instance(instance), _refCounter(refCounter) {
                if (refCounter) {
                    refCounter->add();
                }
            }

            virtual ~BasePointer() {
                if (_refCounter) {
                    _refCounter->sub();
                }
            }

            BasePointer(const BasePointer &ptr) : _instance(ptr._instance), _refCounter(ptr._refCounter) {
                if (_refCounter) {
                    _refCounter->add();
                }
            }

            template <typename X>
            BasePointer(const BasePointer<X> &ptr)
                    : _instance(dynamic_cast<T*>(ptr._instance)), _refCounter(ptr._refCounter) {
                if (_refCounter) {
                    _refCounter->add();
                }
            }

            BasePointer(BasePointer &&ptr) noexcept : _instance(ptr._instance), _refCounter(ptr._refCounter) {
                ptr._instance = nullptr;
                ptr._refCounter = nullptr;
            }

            template <typename X>
            BasePointer(BasePointer<X> &&ptr) noexcept
                    : _instance(dynamic_cast<T*>(ptr._instance)), _refCounter(ptr._refCounter) {
                ptr._instance = nullptr;
                ptr._refCounter = nullptr;
            }

            void _set(const BasePointer &ptr) {
                if (_refCounter) {
                    _refCounter->sub();
                }
                _instance = ptr._instance;
                _refCounter = ptr._refCounter;
                if (_refCounter) {
                    _refCounter->add();
                }
            }

            template <typename X>
            void _set(const BasePointer<X> &ptr) {
                if (_refCounter) {
                    _refCounter->sub();
                }
                _instance = dynamic_cast<T*>(ptr._instance);
                _refCounter = ptr._refCounter;
                if (_refCounter) {
                    _refCounter->add();
                }
            }

            void _move(const BasePointer &ptr) {
                if (_refCounter) {
                    _refCounter->sub();
                }
                _instance = ptr._instance;
                _refCounter = ptr._refCounter;
                ptr._instance = nullptr;
                ptr._refCounter = nullptr;
            }

            template <typename X>
            void _move(const BasePointer &ptr) {
                if (_refCounter) {
                    _refCounter->sub();
                }
                _instance = dynamic_cast<T*>(ptr._instance);
                _refCounter = ptr._refCounter;
                ptr._instance = nullptr;
                ptr._refCounter = nullptr;
            }

            void _swap(BasePointer &ptr) {
                std::swap(_instance, ptr._instance);
                std::swap(_refCounter, ptr._refCounter);
            }

            template <typename X>
            void _swap(BasePointer<X> &ptr) {
                auto i = dynamic_cast<X*>(_instance);
                _instance = dynamic_cast<T*>(ptr._instance);
                ptr._instance = i;

                std::swap(_refCounter, ptr._refCounter);
            }
        };
    }

    template <typename T>
    class rs_ptr : private __private__::BasePointer<T> {
        template <typename X>
        friend class rs_ptr;

        using base = __private__::BasePointer<T>;

        rs_ptr(T *instance, __private__::IRefCounter *refCounter) : base(instance, refCounter) {}

    public:
        using type = T;

        rs_ptr() : base() {}

        rs_ptr(const rs_ptr &ptr) : base(ptr) {}

        template <typename X>
        rs_ptr(const rs_ptr<X> &ptr) : base(ptr) {}

        rs_ptr(rs_ptr &&ptr) noexcept : base(std::move(ptr)) {}

        template <typename X>
        rs_ptr(rs_ptr<X> &&ptr) noexcept : base(std::move(ptr)) {}

        ~rs_ptr() override = default;

        rs_ptr &operator=(const rs_ptr &ptr) {
            if (this != &ptr) {
                base::_set(ptr);
            }
            return *this;
        }

        template <typename X>
        rs_ptr &operator=(const rs_ptr<X> &ptr) {
            if (this != &ptr) {
                base::_set(ptr);
            }
            return *this;
        }

        rs_ptr &operator=(rs_ptr &&ptr) noexcept {
            if (this != &ptr) {
                base::_move(ptr);
            }
            return *this;
        }

        template <typename X>
        rs_ptr &operator=(rs_ptr<X> &&ptr) noexcept {
            if (this != &ptr) {
                base::_move(ptr);
            }
            return *this;
        }

        rs_ptr &operator=(nullptr_t) {
            if (base::_refCounter) {
                base::_refCounter->sub();
            }
            base::_instance = nullptr;
            base::_refCounter = nullptr;
        }

        T *get() const {
            return base::_instance;
        }

        T *operator->() const {
            return get();
        }

        T &operator*() const {
            return *get();
        }

        [[nodiscard]] bool isNull() const {
            return get() == nullptr;
        }

        [[nodiscard]] bool isNotNull() const {
            return get() != nullptr;
        }

        explicit operator bool() const {
            return isNotNull();
        }

        bool operator!() const {
            return isNull();
        }


        template <typename To>
        rs_ptr static_cast_to() {
            return rs_ptr(static_cast<To*>(base::_instance), base::_refCounter);
        }

        template <typename To>
        rs_ptr dynamic_cast_to() {
            return rs_ptr(dynamic_cast<To*>(base::_instance), base::_refCounter);
        }

        template <typename To>
        rs_ptr const_cast_to() {
            return rs_ptr(const_cast<To*>(base::_instance), base::_refCounter);
        }

        template <typename To>
        rs_ptr reinterpret_cast_to() {
            return rs_ptr(reinterpret_cast<To*>(base::_instance), base::_refCounter);
        }


        template <typename ... Args>
        static rs_ptr<T> alloc(Args &&... args) {
            auto rc = new __private__::TypedRefCounter<T>;
            auto instance = rc->allocate(std::forward<Args>(args)...);
            return rs_ptr<T>(instance, rc);
        }


        rs_ptr<T> copy() const {
            return isNull() ? rs_ptr<T>() : alloc(*get());
        }

    };


    /// optional implementation
    template <typename T>
    class rs_optional {
        rs_ptr<T> value;

    public:
        rs_optional() = default;

        explicit rs_optional(const T &initValue) {
            value = rs_ptr<T>::alloc(initValue);
        }

        rs_optional(const rs_optional &o) {
            value = o.value.copy();
        }

        rs_optional(rs_optional &&o) noexcept : value(std::move(o.value)) {}

        rs_optional &operator=(const rs_optional &o) {
            if (this != &o) {
                value = o.value.copy();
            }
            return *this;
        }

        rs_optional &operator=(rs_optional &&o) noexcept {
            if (this != &o) {
                value = std::move(o.value);
            }
            return *this;
        }

        T &get() const {
            return *value.get();
        }

        T &operator*() const {
            return get();
        }

        T *operator->() const {
            return value.get();
        }

        explicit operator T &() const {
            return get();
        }

        [[nodiscard]] bool isPresent() const {
            return value.isNotNull();
        }

        [[nodiscard]] bool isNotPresent() const {
            return value.isNull();
        }

        explicit operator bool() const {
            return isPresent();
        }

        bool operator!() const {
            return isNotPresent();
        }

    };


    /// variant implementation
    namespace __private__ {
        template <size_t N, typename ... Types>
        struct variant_tuple_impl;

        template <size_t N, typename T, typename ... Types>
        struct variant_tuple_impl<N, T, Types...> {
            using type = T;
            using base = variant_tuple_impl<N + 1, Types...>;
            static constexpr auto INDEX = N;
        };

        template <size_t N, typename T>
        struct variant_tuple_impl<N, T> {
            using type = T;
            using base = void;
            static constexpr auto INDEX = N;
        };


        template <typename, typename ...>
        struct variant_tuple_index_of;

        template <typename Type, typename T, typename ... Types>
        struct variant_tuple_index_of<Type, T, Types...> {
            static constexpr auto value = std::is_same<Type, T>::value
                    ? 1
                    : (variant_tuple_index_of<Type, Types...>::value + 1);
        };

        template <typename Type, typename T>
        struct variant_tuple_index_of<Type, T> {
            static constexpr auto value = std::is_same<Type, T>::value
                    ? 1
                    : 0;
            static_assert(value != 0, "tuple not contains this type");
        };


        template <typename ... Types>
        struct variant_tuple {
            using self_t = variant_tuple_impl<0, Types...>;

            struct SimpleRefCounter : IRefCounter {
                variant_tuple<Types...> *tuple;
                SimpleRefCounter(variant_tuple<Types...> *tuple) : tuple(tuple) {}
            private:
                void onReleaseReference() override {
                    tuple->_free<self_t>();
                }
            };

            size_t index;
            BasePointer<void> container;

            template <typename Tuple, typename Type>
            void _set(const Type &newValue) {
                using type = typename Tuple::type;
                using base = typename Tuple::base;
                if constexpr (std::is_same<type, Type>::value) {
                    container._instance = new Type{newValue};
                    container._refCounter = new SimpleRefCounter{this};
                    index = Tuple::INDEX;
                } else if constexpr (!std::is_void<base>::value) {
                    _set<base, Type>(newValue);
                }
            }

            template <typename Tuple, typename Type>
            void _get(Type **result) const {
                using type = typename Tuple::type;
                using base = typename Tuple::base;
                if (index == Tuple::INDEX) {
                    *result = reinterpret_cast<Type*>(container._instance);
                } else {
                    if constexpr (!std::is_void<base>::value) {
                        _get<base, Type>(result);
                    }
                }
            }

            template <typename Tuple>
            void _free() {
                using type = typename Tuple::type;
                using base = typename Tuple::base;
                if (index == Tuple::INDEX) {
                    delete reinterpret_cast<type*>(container._instance);
                    container._instance = nullptr;
                    container._refCounter = nullptr;
                } else {
                    if constexpr (!std::is_void<base>::value) {
                        _free<base>();
                    }
                }
            }

            template <typename Tuple>
            void _copy(void *from) {
                using type = typename Tuple::type;
                using base = typename Tuple::base;
                if (index == Tuple::INDEX) {
                    auto value = reinterpret_cast<type*>(from);
                    *reinterpret_cast<type**>(&container._instance) = new type{*value};
                } else {
                    if constexpr (!std::is_void<base>::value) {
                        _copy<base>(from);
                    }
                }
            }

            template <typename Tuple, typename Type>
            static size_t _index_of() {
                using type = typename Tuple::type;
                using base = typename Tuple::base;
                if constexpr (std::is_same<type, Type>::value) {
                    return Tuple::INDEX;
                } else if constexpr (std::is_void<base>::value) {
                    return -1;
                } else {
                    return _index_of<base, Type>();
                }
            }

            variant_tuple() : index(-1), container(nullptr, nullptr) {}

            variant_tuple(const variant_tuple &vt) : index(vt.index) {
                _copy<self_t>(vt.container);
            }

            variant_tuple(variant_tuple &&vt) noexcept : index(vt.index), container(std::move(vt.container)) {}

            variant_tuple &operator=(const variant_tuple &vt) {
                if (this != &vt) {
                    if (container._refCounter) {
                        container._refCounter->sub();
                    }
                    index = vt.index;
                    container._refCounter = new SimpleRefCounter{this};
                    _copy<self_t>(vt.container._instance);
                }
                return *this;
            }

            variant_tuple &operator=(variant_tuple &&vt) noexcept {
                if (this != &vt) {
                    container._move(vt.container);
                    index = -1;
                    std::swap(index, vt.index);
                }
            }

            ~variant_tuple() = default;

            template <typename Type>
            void set(const Type &newValue) {
                if (container._refCounter) {
                    container._refCounter->sub();
                }
                _set<self_t, Type>(newValue);
            }

            template <typename Type>
            Type *get() const {
                Type *result = nullptr;
                if (container) {
                    _get<self_t, Type>(&result);
                }
                return result;
            }

            template <typename Type>
            bool is() const {
                return _index_of<self_t, Type>() == index;
            }
        };
    }

    template <typename ... Types>
    class rs_variant {
        static_assert(sizeof...(Types) > 0, "rs_variant should contains one or more types");

        using tuple_type = __private__::variant_tuple<Types...>;

        tuple_type t;

    public:
        template <typename T>
        rs_variant(const T &value) {
            t.set(value);
        }

        rs_variant(const rs_variant &v) : t(v.t) {}

        rs_variant(rs_variant &&v) noexcept : t(std::move(v.t)) {}

        rs_variant &operator=(const rs_variant &v) {
            if (this != &v) {
                t = v.t;
            }
            return *this;
        }

        rs_variant &operator=(rs_variant &&v) noexcept {
            if (this != &v) {
                t = std::move(v.t);
            }
            return *this;
        }

        ~rs_variant() = default;

        template <typename T>
        rs_variant &operator=(const T &newValue) {
            t.set(newValue);
            return *this;
        }

        template <typename T>
        T &get() const {
            return *t.template get<T>();
        }

        [[nodiscard]] size_t index() const {
            return t.index;
        }

        template <typename T>
        [[nodiscard]] bool is() const {
            return t.template is<T>();
        }
    };


    /// primitives
    using rs_int1 = int8_t;
    using rs_int2 = int16_t;
    using rs_int4 = int32_t;
    using rs_int8 = int64_t;
    using rs_uint1 = uint8_t;
    using rs_uint2 = uint16_t;
    using rs_uint4 = uint32_t;
    using rs_uint8 = uint64_t;
    using rs_float4 = float;
    using rs_float8 = double;
    using rs_bool = bool;
    using rs_string = std::u16string;
    using rs_char = char16_t;

    using rs_size = rs_int4;


    /// runtime
    namespace __private__ {
        struct i_rs_type_registry;
        struct i_rs_environment;
        struct i_rs_vm;
        struct i_rs_function;
    }

    using rs_type_registry = __private__::i_rs_type_registry*;

    using rs_env_t = __private__::i_rs_environment*;

    using rs_vm_t = __private__::i_rs_vm*;

    using rs_func = __private__::i_rs_function*;


    struct __private__::i_rs_type_registry {
        // TODO: implement type registry
        virtual ~i_rs_type_registry() = default;

#define define_method(ret, name, ...)\
        virtual ret name(__VA_ARGS__) = 0

        define_method(rs_bool, apply);

#undef define_method
    };


    struct __private__::i_rs_environment {
        virtual ~i_rs_environment() = default;

#define define_method(ret, name, ...)\
        virtual ret name(__VA_ARGS__) = 0

        define_method(rs_func, findFunction, const rs_string &name, const rs_string &signature);

        define_method(void, invoke, rs_func func, void *args);

        template <typename ... Args>
        void invoke2(rs_func func, Args &&... args) {
            // TODO: implement this
        }

        define_method(void, popFromStack, rs_int1 &value);
        define_method(void, popFromStack, rs_int2 &value);
        define_method(void, popFromStack, rs_int4 &value);
        define_method(void, popFromStack, rs_int8 &value);
        define_method(void, popFromStack, rs_uint1 &value);
        define_method(void, popFromStack, rs_uint2 &value);
        define_method(void, popFromStack, rs_uint4 &value);
        define_method(void, popFromStack, rs_uint8 &value);
        define_method(void, popFromStack, rs_float4 &value);
        define_method(void, popFromStack, rs_float8 &value);
        define_method(void, popFromStack, rs_bool &value);
        define_method(void, popFromStack, rs_string &value);
        define_method(void, popFromStack, rs_char &value);

        template <typename T>
        T popFromStack() {
            auto result = T();
            popFromStack(result);
            return result;
        }

        define_method(rs_string, toString, rs_int1 &value);
        define_method(rs_string, toString, rs_int2 &value);
        define_method(rs_string, toString, rs_int4 &value);
        define_method(rs_string, toString, rs_int8 &value);
        define_method(rs_string, toString, rs_uint1 &value);
        define_method(rs_string, toString, rs_uint2 &value);
        define_method(rs_string, toString, rs_uint4 &value);
        define_method(rs_string, toString, rs_uint8 &value);
        define_method(rs_string, toString, rs_float4 &value);
        define_method(rs_string, toString, rs_float8 &value);
        define_method(rs_string, toString, rs_bool &value);
        define_method(rs_string, toString, rs_char &value);

        define_method(rs_vm_t, getVM);

#undef define_method
    };


    struct __private__::i_rs_vm {
        virtual ~i_rs_vm() = default;

#define define_method(ret, name, ...)\
        virtual ret name(__VA_ARGS__) = 0

        define_method(rs_int4, getAPIVersion);

        define_method(rs_env_t, getEnv);

        define_method(rs_int4, destroy);

#undef define_method
    };

    struct __private__::i_rs_function {
        virtual ~i_rs_function() = default;
    };

}

#endif //__rhinestone_api_DBA0D4CF6E944D8C9C5C128163D467BB_hpp__
